// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'real_time_sub_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RealTimeSubway _$RealTimeSubwayFromJson(Map<String, dynamic> json) {
  return RealTimeSubway(
    errorMessage: json['errorMessage'] == null
        ? null
        : ErrorMessage.fromJson(json['errorMessage'] as Map<String, dynamic>),
    realtimeArrivalList: (json['realtimeArrivalList'] as List<dynamic>?)
        ?.map((e) => RealtimeArrivalList.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$RealTimeSubwayToJson(RealTimeSubway instance) =>
    <String, dynamic>{
      'errorMessage': instance.errorMessage,
      'realtimeArrivalList': instance.realtimeArrivalList,
    };

ErrorMessage _$ErrorMessageFromJson(Map<String, dynamic> json) {
  return ErrorMessage(
    status: json['status'] as int?,
    code: json['code'] as String?,
    message: json['message'] as String?,
    link: json['link'] as String?,
    developerMessage: json['developerMessage'] as String?,
    total: json['total'] as int,
  );
}

Map<String, dynamic> _$ErrorMessageToJson(ErrorMessage instance) =>
    <String, dynamic>{
      'status': instance.status,
      'code': instance.code,
      'message': instance.message,
      'link': instance.link,
      'developerMessage': instance.developerMessage,
      'total': instance.total,
    };

RealtimeArrivalList _$RealtimeArrivalListFromJson(Map<String, dynamic> json) {
  return RealtimeArrivalList(
    subwayId: json['subwayId'] as String?,
    updnLine: json['updnLine'] as String?,
    trainLineNm: json['trainLineNm'] as String?,
    subwayHeading: json['subwayHeading'] as String?,
    statnFid: json['statnFid'] as String?,
    statnTid: json['statnTid'] as String?,
    statnId: json['statnId'] as String?,
    statnNm: json['statnNm'] as String?,
    ordkey: json['ordkey'] as String?,
    subwayList: json['subwayList'] as String?,
    statnList: json['statnList'] as String?,
    btrainSttus: json['btrainSttus'] as String?,
    barvlDt: json['barvlDt'] as String?,
    btrainNo: json['btrainNo'] as String?,
    bstatnNm: json['bstatnNm'] as String?,
    recptnDt: json['recptnDt'] == null
        ? null
        : DateTime.parse(json['recptnDt'] as String),
    arvlMsg2: json['arvlMsg2'] as String?,
    arvlMsg3: json['arvlMsg3'] as String?,
    arvlCd: json['arvlCd'] as String?,
  );
}

Map<String, dynamic> _$RealtimeArrivalListToJson(
        RealtimeArrivalList instance) =>
    <String, dynamic>{
      'subwayId': instance.subwayId,
      'updnLine': instance.updnLine,
      'trainLineNm': instance.trainLineNm,
      'subwayHeading': instance.subwayHeading,
      'statnFid': instance.statnFid,
      'statnTid': instance.statnTid,
      'statnId': instance.statnId,
      'statnNm': instance.statnNm,
      'ordkey': instance.ordkey,
      'subwayList': instance.subwayList,
      'statnList': instance.statnList,
      'btrainSttus': instance.btrainSttus,
      'barvlDt': instance.barvlDt,
      'btrainNo': instance.btrainNo,
      'bstatnNm': instance.bstatnNm,
      'recptnDt': instance.recptnDt?.toIso8601String(),
      'arvlMsg2': instance.arvlMsg2,
      'arvlMsg3': instance.arvlMsg3,
      'arvlCd': instance.arvlCd,
    };
