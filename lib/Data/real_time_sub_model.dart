import 'package:json_annotation/json_annotation.dart';

part 'real_time_sub_model.g.dart';

@JsonSerializable()
class RealTimeSubway {
    RealTimeSubway({
        this.errorMessage,
        required this.realtimeArrivalList,
    });

    ErrorMessage? errorMessage;
    List<RealtimeArrivalList>? realtimeArrivalList;

    factory RealTimeSubway.fromJson(
      Map<String, dynamic> json) => _$RealTimeSubwayFromJson(json);

    Map<String, dynamic> toJson() => _$RealTimeSubwayToJson(this);

}

@JsonSerializable()
class ErrorMessage {
    ErrorMessage({
      this.status,
      this.code,
      this.message,
      this.link,
      this.developerMessage,
      required this.total,
    });

    int? status;
    String? code;
    String? message;
    String? link;
    String? developerMessage;
    int total;

    factory ErrorMessage.fromJson(
      Map<String, dynamic> json) => _$ErrorMessageFromJson(json);

    Map<String, dynamic> toJson() => _$ErrorMessageToJson(this);

}

@JsonSerializable()
class RealtimeArrivalList {
    RealtimeArrivalList({
      this.subwayId,
      this.updnLine,
      this.trainLineNm,
      this.subwayHeading,
      this.statnFid,
      this.statnTid,
      this.statnId,
      this.statnNm,
      this.ordkey,
      this.subwayList,
      this.statnList,
      this.btrainSttus,
      this.barvlDt,
      this.btrainNo,
      this.bstatnNm,
      this.recptnDt,
      this.arvlMsg2,
      this.arvlMsg3,
      this.arvlCd,
    });

    String? subwayId;
    String? updnLine;  // UpdnLine? updnLine;
    String? trainLineNm;
    String? subwayHeading;// SubwayHeading? subwayHeading;
    String? statnFid;
    String? statnTid;
    String? statnId;
    String? statnNm;
    String? ordkey;
    String? subwayList;
    String? statnList;
    String? btrainSttus;
    String? barvlDt;
    String? btrainNo;
    String? bstatnNm;
    DateTime? recptnDt;
    String? arvlMsg2;
    String? arvlMsg3;
    String? arvlCd;

    factory RealtimeArrivalList.fromJson(
      Map<String, dynamic> json) => _$RealtimeArrivalListFromJson(json);

    Map<String, dynamic> toJson() => _$RealtimeArrivalListToJson(this);
}