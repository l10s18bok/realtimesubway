import 'package:realtime_subway/Data/real_time_sub_model.dart';
import 'dart:ui';
class ResourceTranslate {
  RealtimeArrivalList? model;

  ResourceTranslate({
    this.model,
  });

  get trainStatus {
    Map<String,String> dict = {
      '0' : '진입',
      '1' : '도착',
      '2' : '출발',
      '3' : '전역출발',
      '4' : '전역진입',
      '5' : '전역도착',
      '99' : '운행중'
    };
    return dict[this.model!.arvlCd];
  }

  get subwayLineColor {
    Map<String, Color> dict = {
      '1001' : Color(0xFF0D3692),
      '1002' : Color(0xFF33A23D),
      '1003' : Color(0xFFFE5D10),
      '1004' : Color(0xFF00A2D1),
      '1005' : Color(0xFF8B50A4),
      '1006' : Color(0xFFC55C1D),
      '1007' : Color(0xFF54640D),
      '1008' : Color(0xFFF14C82),
      '1009' : Color(0xFFAA9872),
      '1063' : Color(0xFF73C7A6), //경의선
      '1075' : Color(0xFFFF8C00), //분당선,수인선
      '1067' : Color(0xFF32C6A6), //경춘선
      '1065' : Color(0xFF3681B7), //공항철도
      '1071' : Color(0xFF8CADCB), //인천1호선
      '1077' : Color(0xFFC82127), //신분당선
      '1092' : Color(0xFFB7C452), //우이신설
      '인천2호선' : Color(0xFFED8000),
      '의정부경전철' : Color(0xFFFDA600),
      '용인경전철' : Color(0xFF4EA364),
      '인천자기부상' : Color(0xFFFFCD12),
    };
    return dict[this.model!.subwayId];
  }

    String subwayLine({String? id}) {
    Map<String, String> dict = {
      '1001' : '1',
      '1002' : '2',
      '1003' : '3',
      '1004' : '4',
      '1005' : '5',
      '1006' : '6',
      '1007' : '7',
      '1008' : '8',
      '1009' : '9',
      '1063' : '경의', //경의선
      '1075' : '분당', //분당선,수인선
      '1067' : '경춘', //경춘선
      '1065' : '공항', //공항철도
      '1071' : '인천', //인천1호선
      '1077' : '신분당', //신분당선
      '1092' : '우이' //우이신설
    };
    return dict[id].toString();
  }

  String currentStatus() {
    final curState = this.trainStatus;
    final arrivalMilliseconds = int.parse(model!.barvlDt as String);
    final arrivalTime = arrivalMilliseconds > 60 ? 
                        arrivalMilliseconds / 60 : arrivalMilliseconds;

    //print('===> arrivalMilliseconds : $arrivalMilliseconds     arrivalTime: $arrivalTime');
    String trainStatus = arrivalMilliseconds > 60 ? '${arrivalTime.toInt()} 분후 도착' : '열차가 들어오고 있습니다.';
    if (curState == '도착') trainStatus = '탑승 대기중';
    else if (curState == '전역도착' && arrivalMilliseconds == 0) trainStatus = '전역 출발전';
    else if (curState == '출발') trainStatus = '열차가 출발하였습니다.';
    else if (curState == '운행중' && arrivalMilliseconds == 0) trainStatus = '도착시간 알수 없음';

    return trainStatus;
  }

}
