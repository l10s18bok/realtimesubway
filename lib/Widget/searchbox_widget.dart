import 'package:flutter/material.dart';
import 'package:realtime_subway/Widget/shimmer_widget.dart';

class SearchBoxWidget extends StatefulWidget {
  final bool isShimmerAction;
  final int maxLines;
  final String label;
  final String text;
  final ValueChanged<String> onChanged;
  final ValueChanged<String> onSubmitted;
  final VoidCallback onClear;

  const SearchBoxWidget({
    Key? key,
    this.isShimmerAction = false,
    this.maxLines = 1,
    required this.label,
    required this.text,
    required this.onChanged,
    required this.onSubmitted,
    required this.onClear,
  }) : super(key: key);

  @override
  _SearchBoxWidgetState createState() => _SearchBoxWidgetState();
}

class _SearchBoxWidgetState extends State<SearchBoxWidget> {
  late final TextEditingController controller;

  @override
  void initState() {
    super.initState();

    controller = TextEditingController(text: widget.text);
  }

  @override
  void dispose() {
    controller.dispose();

    super.dispose();
  }

  Widget renderShimmerInputField() {
    return ShimmerWidget.circular(
        height: 53, 
        width: double.infinity,
        shapeBorder: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(21.5)),
    );  
  }

    Widget renderInputField() {
    final hintColor = Color(0xFFB2ACFF);
    final suffixIcon = GestureDetector(
        onTap: () {
          controller.clear();
          widget.onClear();
        },
        child: Container(
          width: 25.0,
          height: 25.0,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: Color(0xFFD8D8D8),
          ),
          child: Icon(
            Icons.close,
            size: 20.0,
            color: Colors.black,
          ),
        ),
      ); 

    final baseInputDecoration = InputDecoration(
      hintText: "역검색 : 서울(ㅇ) 서울역(x)",
      hintStyle: TextStyle(
        color: hintColor,
        fontSize: 15,
      ),
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(21.5),
        ),
      suffix: suffixIcon,   
      contentPadding: EdgeInsets.only(left: 16,right: 10),
    );

    return TextField(
        controller: controller,
        textAlignVertical: TextAlignVertical.center,
        textInputAction: TextInputAction.search,
        decoration: baseInputDecoration,
        style: null,
        maxLines: widget.maxLines,
        onChanged: widget.onChanged,
        onSubmitted: widget.onSubmitted,
    );
  }  

  @override
  Widget build(BuildContext context) => widget.isShimmerAction ?
    renderShimmerInputField() :
    renderInputField();
}
