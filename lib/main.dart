import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:realtime_subway/Data/real_time_sub_model.dart';
import 'package:realtime_subway/Data/resource_translate.dart';
import 'package:realtime_subway/Retrofit/RestClient.dart';
import 'package:realtime_subway/Widget/searchbox_widget.dart';
import 'package:realtime_subway/Widget/shimmer_widget.dart';
import 'package:realtime_subway/detail_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      title: 'real-time subway',
      theme: ThemeData(
        brightness: Brightness.dark,
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: '실시간 지하철 도착정보'),
      routes: {
        DetailScreen.routeName: (context) => DetailScreen(),
      }
      //darkTheme: ThemeData.dark(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  late RestClient client;
  ScrollController scrollController = ScrollController();

  List<RealtimeArrivalList>? data;

  String searchStation = ''; // 초기값 없으면 현재시간 전체 지하철 운행정보 로딩

  int listTotal = 0;

  int startPage = 0;

  final pageSize = 10;

  bool isLoading = false;
  bool endData = false;

  final ResourceTranslate resourceTranslate = ResourceTranslate();

  @override
  void initState() {
    super.initState();
    Dio dio = Dio();

    client = RestClient(dio);

    scrollController.addListener(scrollListener);

    loadData();

  }

  @override
  void dispose() { 
    scrollController.dispose();
    super.dispose();
  }

  scrollListener() {
    if(endData) return;

    if (scrollController.position.pixels == scrollController.position.maxScrollExtent) {
      loadData();
    }
  }

  loadData() async{
    if (isLoading || endData) return;

    setState(() => isLoading = true);
    await Future.delayed(Duration(seconds: 1));

    final start = startPage == 0 ? startPage : startPage + 1;

    searchStation = searchStation.trim();

    if (searchStation.isNotEmpty) {
      if (searchStation.substring(searchStation.length -1 ,searchStation.length) == '역') {
        searchStation = searchStation.replaceRange(searchStation.length -1,searchStation.length, '');
      }
    }

    try {
      print('starPage = $start   endPage = ${(startPage + pageSize)}    total = $listTotal    searchWord = $searchStation');
      final resp = await client.realTimeSubwayData(start: start, end: startPage + pageSize, station: searchStation);
      
      if (resp.errorMessage == null) {
        showSnackBar(message: '$searchStation에 대한정보가 없습니다.',color: Colors.deepOrange);
        setState(() => isLoading = false);
        data = [];
        return;
      }

      if (resp.errorMessage?.status == 200) {
        listTotal = resp.errorMessage!.total;
        
        if (data == null) data = [];
        data!.addAll(resp.realtimeArrivalList!);

        if(listTotal <= data!.length) 
          endData = true;

        startPage = startPage + pageSize;
        
        setState(() => isLoading = false);
      } else {
        showSnackBar(message: '에러 : ${resp.errorMessage!.message}',color: Colors.red);
        setState(() => isLoading = false);
      }

    } on DioError catch(e) {
      setState(() => isLoading = false);
      showSnackBar(message: 'ERROR ${e.response.toString()}',color: Colors.black);
    }

  }

  showSnackBar({required String message, Color? color}) {
    final maxDuration = Duration(seconds: 2);
      final snackBar = SnackBar(
        content: Text(message),
        duration: maxDuration,
        backgroundColor: color,
      );
     ScaffoldMessenger.of(context)
     ..removeCurrentSnackBar()
     ..showSnackBar(snackBar); 
  }

  Future<void> refersh() async{
    FocusScope.of(context).requestFocus(FocusNode());
    startPage = 0;
    listTotal = 0;
    endData = false;
    data = null;
    loadData();
    return null;
  }

  Widget renderCardShimmer() {
    return Card(
      child: ListTile(
        leading: ShimmerWidget.circular(
          width: 40, 
          height: 40,
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12),
        ),
        title: Align(
          alignment: Alignment.centerLeft,
          child: ShimmerWidget.rectangular(
            height: 16, 
            width: MediaQuery.of(context).size.width / 4,
          ),
        ),
        subtitle: Align(
          alignment: Alignment.centerLeft,
          child: ShimmerWidget.rectangular(
            height: 14, 
            width: MediaQuery.of(context).size.width / 2,
          ),
        ),
      ),
    );
  }

  Widget renderCard({required int index}) {
    resourceTranslate.model = data?[index];
    return Card(
      child: ListTile(
        onTap: () {
          FocusScope.of(context).requestFocus(FocusNode());
          resourceTranslate.model = data?[index];
          Navigator.pushNamed(context, DetailScreen.routeName,arguments: resourceTranslate);
        },

        leading: CircleAvatar(
          backgroundColor: resourceTranslate.subwayLineColor,
          child: Text('${resourceTranslate.subwayLine(id: resourceTranslate.model!.subwayId)}',
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
            ),
            maxLines: 1,
            textAlign: TextAlign.center,
          ),
        ),
        title: Text(
          resourceTranslate.currentStatus(),
          maxLines: 1,
        ),
        subtitle: Text(resourceTranslate.model!.trainLineNm ?? ''),
        trailing: Text(
          resourceTranslate.trainStatus,
          style: TextStyle(
            color: Colors.red,
            fontSize: 16,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }

  Widget renderShimmerList() {
    return Expanded(
      child: ListView.separated(
        separatorBuilder: (context, index) {
          return Divider(height: 10, color: Colors.grey);
        }, 
        itemCount: 20,
        itemBuilder: (BuildContext context, int index) {
          return renderCardShimmer();
        }, 
      ),
    );
  }

  Widget renderListView() {
    if (data!.isEmpty) 
      return Expanded(
        child: Center(
          child: Text('제공된 데이터가 없습니다.'),
        ),
      );

    return Expanded(
      child: RefreshIndicator(
        onRefresh: () => refersh(),
        child: ListView.separated(
          controller: scrollController,
          separatorBuilder: (context, index) {
            return Divider(height: 10, color: Colors.grey);
          }, 
          itemCount: (data == null) || (data!.isEmpty) ? 20 : data!.length,
          itemBuilder: (BuildContext context, int index) {
            if(isLoading) {
              return renderCardShimmer();
              
            } else {
              return renderCard(index: index);
            }
            
          }, 
        ),
      ),
    );
  }

  Widget renderSearchBox() {

    return Container(
      padding: const EdgeInsets.fromLTRB(20, 0, 20, 10),
      height: 53,
      child: SearchBoxWidget(
        label: '역검색',
        text: searchStation,
        isShimmerAction: data == null, 
        onChanged: (val) {
            searchStation = val;
        }, 
        onSubmitted: (val) => refersh(),
        onClear: () => searchStation = '',
      )
    );
  }

  Widget renderListTotal() {
    return Padding(
      padding: const EdgeInsets.only(top: 10,left: 5),
      child: data == null ?
        Row(
          children: [
            ShimmerWidget.rectangular(
              height: 20, 
              width: MediaQuery.of(context).size.width / 3,
            ),
          ],
        ) :
        Row(
          children: [
            Text('총 검색리스트 : '),
            Text(
              '$listTotal',
              style: TextStyle(fontWeight: FontWeight.bold, color: Colors.purpleAccent),
              ),
            Text(' 개'),
          ],
        ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black87,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        title: Text(widget.title),
        centerTitle: true,
        actions: [
          IconButton(
            onPressed: refersh, 
            icon: Icon(Icons.refresh))
        ],
      ),
      body: Column(
        children: [
          renderSearchBox(),
          renderListTotal(),
          (isLoading) && (data == null) ? renderShimmerList() : renderListView(),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        mini: true,
        onPressed: refersh,
        tooltip: 'search',
        child: Icon(Icons.search_sharp),
      ),
    );
  }
}
