import 'package:realtime_subway/Data/real_time_sub_model.dart';
import 'package:retrofit/retrofit.dart';
import 'package:dio/dio.dart';

part 'RestClient.g.dart';

@RestApi(baseUrl: 'http://swopenapi.seoul.go.kr/api/subway/4955545a586c313038336f476f456f')

abstract class RestClient {
  factory RestClient(Dio dio,{String baseUrl}) = _RestClient;

  @GET('/json/realtimeStationArrival/{start}/{end}/{station}')
  Future<RealTimeSubway> realTimeSubwayData({@Path() int start = 0, @Path() int end = 10, @Path() required String station});
}

