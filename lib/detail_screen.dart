import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'Data/resource_translate.dart';

class DataInformation {
  String? parameter;
  String? info;
  String? value;

  DataInformation({
    this.parameter,
    this.info,
    this.value,
  });
  
}

class DetailScreen extends StatelessWidget {
  const DetailScreen({ Key? key }) : super(key: key);

  static const routeName = '/detailArguments';

  @override
  Widget build(BuildContext context) {
  final Map<String,String> mapData = {
      'subwayId' : '지하철호선ID',
      'updnLine' : '상하행선구분',
      'trainLineNm' : '도착지방면',
      'subwayHeading' : '내리는문방향',
      'statnFid' : '이전지하철역ID',
      'statnTid' : '다음지하철역ID',
      'statnId' : '지하철역ID',
      'statnNm' : '지하철역명',
      'ordkey' : '도착예정열차순번',
      'subwayList' : '연계호선',
      'statnList' : '연계지하철역ID',
      'btrainSttus' : '열차종류',
      'barvlDt' : '열차도착예정시간\n(단위:초)',
      'btrainNo' : '열차번호',
      'bstatnNm' : '종착지하철역명',
      'recptnDt' : '도착정보 생성시간',
      'arvlMsg2' : '첫번째도착메세지',
      'arvlMsg3' : '두번째도착메세지',
      'arvlCd' : '도착코드',
    };

    final arrivalList = ModalRoute.of(context)!.settings.arguments as ResourceTranslate;

    List<DataInformation> dataList = [];

    mapData.forEach((key, info) {
      final DataInformation dataInfo = DataInformation(parameter: key, info: info);
      dataList.add(dataInfo);
    });

    dataList.forEach((model) {
      if ('${model.parameter}' == 'recptnDt') 
        model.value = DateFormat('yyyy.MM.dd  HH시 MM분').format(arrivalList.model!.recptnDt!).toString();
      else if ('${model.parameter}' == 'subwayId')
        model.value = '${arrivalList.subwayLine(id: arrivalList.model!.subwayId)}호선';  
      else 
       model.value = arrivalList.model!.toJson()['${model.parameter}'];
    });

    
    return Scaffold(
      appBar: AppBar(title: Text('상세페이지'),),
      body: SingleChildScrollView(
        child: Container(
          child : DataTable(
            headingRowColor: MaterialStateColor.resolveWith((states) => Colors.blue),
            columnSpacing: 15.0,
            columns: [
            DataColumn(
              label: Text('변수명')),
            DataColumn(label: Text('변수설명')),
            DataColumn(label: Text('상태값')),
          ], rows: dataList.map((DataInformation d) => DataRow(
                cells: [
                  DataCell(Text(d.parameter ?? '')),
                  DataCell(Text(d.info ?? '')),
                  DataCell(Text(d.value ?? '')),
                ],
            )).toList(),
          ),
        ),
      ),
    );
  }
}